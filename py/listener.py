#!/usr/bin/env python
from __future__ import print_function
import os
import time
import argparse

root_path = '/global/scratch/sd/jmustafa/quantum-crater/'
triggers_path = os.path.join(root_path, 'py/triggers')

def print_header():
    print('='*60)
    print()
    print('quantum-crater')
    print()
    print('Started the "listener"')
    print()
    print('='*60)

def start(args):
    before = dict([(f, None) for f in os.listdir(triggers_path)])
    time1 = sorted([int(f) for f in os.listdir(triggers_path)])[-1]
    while 1:
        try:
            time.sleep(5)
            after = dict([(f, None) for f in os.listdir (triggers_path)])
            added = [f for f in after if not f in before]
            time2 = sorted([int(f) for f in os.listdir(triggers_path)])[-1]
            if time2 > time1:
                time1 = time2
                cmd = ' '.join([os.path.join(root_path, 'py/output2json.py'), os.path.join(root_path, 'qe/sample/al001.rx.out')])
                os.system(cmd)
            before = after

            # if added:
                # for f in added:
                    # print(f)
        except KeyboardInterrupt:
            print('Stopped the "listener"')
            return

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    args = parser.parse_args()

    print_header()

    start(args)

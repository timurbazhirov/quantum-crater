#!/usr/bin/env python
from __future__ import print_function
import time
import argparse
import json
import urllib2
import cStringIO as StringIO

def post_step(step):
    if type(step) != dict:
        print('ERROR: "step" is not a dict')
        return
    # url = 'http://localhost:3000/step'
    url = 'http://quantum-crater.meteor.com/step'

    json_data = json.dumps(step)
    print(json_data)

    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json_data)

def to_mdl_molfile(step):
    zeros8 = (0,0,0,0,0,0,0,0)
    zeros11 = zeros8+(0,0,0)

    atoms = step['atoms']
    natoms = step['natoms']

    sio = StringIO.StringIO()
    sio.write('\n')
    sio.write('\n')
    sio.write('\n')
    sio.write(('%3d'*3+'  0000'+'%3d'*4+'  0999 V2000\n') % ((natoms,0,0)+(0,0,0,0)))
    i=0
    for label in atoms:
        for pos in atoms[label]:
            sio.write(('%10.4f'*3+'%3s'+'%4d'+'%3d'*10+'\n') % (tuple(pos)+(label,)+zeros11))
    # sio.write('  2  1  2  0\n')
    # sio.write('  3  2  1  0\n')
    sio.write('M  END\n')
    sio.write('\n$$$$\n')

    mol = sio.getvalue()

    print(repr(sio.getvalue()))

    return {'molfile':mol, 'time':time.time()}

def read_outfile(fname):
    with open(fname, 'r') as f:
        contents = f.readlines()
    
    pos_header_lines = []
    dlv_header_lines = []
    total_energies = []
    for (iln, line) in enumerate(contents):
        if 'number of atoms/cell' in line:
            nat = int(line.split('=')[1])
        if 'ATOMIC_POSITIONS' in line:
            # print(iln+1)
            pos_header_lines.append(iln)
        if 'CELL_PARAMETERS' in line:
            # print(iln+1)
            dlv_header_lines.append(iln)
        if '!    total energy' in line:
            total_energies.append(float(line.split('=')[1].split()[0]))
    
    nsteps = len(pos_header_lines)
    print(nsteps)
    print(len(pos_header_lines))
    print(len(dlv_header_lines))

    if len(dlv_header_lines) == 1:
        variable_cell = False
    elif len(dlv_header_lines) == len(pos_header_lines):
        variable_cell = True
    else:
        variable_cell = None
    if variable_cell is None:
        print('ERROR: variable_cell is None')
        return
    print('vc=', variable_cell)

    print('nat=', nat)

    steps = []
    for istep in range(nsteps):
        istart_pos = pos_header_lines[istep]
        if variable_cell:
            istart_dlv = dlv_header_lines[istep]
        else:
            istart_dlv = dlv_header_lines[0]
        atoms = {}
        cell = []
        for iatom in range(nat):
            iln = istart_pos + iatom + 1
            line = contents[iln]
            label = line.split()[0].strip()
            pos = map(float, line.split()[1:])
            if label in atoms:
                atoms[label].append(pos)
            else:
                atoms[label] = []
                atoms[label].append(pos)
        for iv in range(3):
            iln = istart_dlv + iv + 1
            line = contents[iln]
            cell.append(map(float, line.split()))
        steps.append({'atoms':atoms, 'cell':cell, 'energy':total_energies[istep], 'natoms':nat})

    return steps

def main(args):
    print('Running output2json.py')
    # return
    # print(args)
    steps = read_outfile(args.outfile)
    # print(to_mdl_molfile(steps[0]))
    post_step(to_mdl_molfile(steps[0]))
    # print(to_mdl_molfile(steps[1]))
    # print(steps)
    # for istep in range(len(steps)):
       # time.sleep(1)
       # post_step(to_mdl_molfile(steps[istep]))
    # post_step(steps[0])
    # post_step(steps[1])

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('outfile')
    args = parser.parse_args()
    main(args)

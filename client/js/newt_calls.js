/**************************************************************************** 
	File contains the functions that do the following:
		- authenticate the user at NERSC
		- upload a file
		- run a jobscript 
	Authors: TB, JM, UB
****************************************************************************/
// Newt urls
var hopper_dir 		= "/global/homes/t/timur/";
var hopper_scratch 	= "/global/scratch/sd/timur/kwantumcoffee/";
var NERSC_URL 		= "http://portal.nersc.gov/project/ucbcmt/outbox";

function get_motd (evt, tmplt){
	$.newt_ajax({type: "GET",
    	url: "/file/carver/etc/motd?view=read",
    	success: function(res){
    	    // res contains raw file data
    	    //$('#file').html(res);
    	    console.log(res, tmplt);
    	    return tmplt.nersc_response;
    	},
	});
}
window.get_motd = get_motd;
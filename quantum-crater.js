var NERSC_PATH = "/global/scratch/sd/jmustafa/quantum-crater";
var SCRIPT_NAME = "/py/output2json.py";
var OUTPUT_FILE = "/qe/sample/al001.rx.out";
var PYTHON_PATH = "/usr/common/usg/python/2.7.3/bin/python";
 
if (Meteor.isClient) {

  Session.setDefault('nersc_response', 'Response will be here');
  Session.setDefault('mongo_vars', 'Mongo updates will be here');
/*
  Meteor.autorun(function () {
    step.find().forEach(function (step) {
      Meteor.subscribe('mongo_vars', step._id);
    });
  });
*/
  Template.hello.greeting = "Try material science in action now!";

  Template.hello.events({
    'click input' : function () {
      // template data, if any, is available in 'this'
      if (typeof console !== 'undefined')
        console.log("You pressed the button");
    }
  });

  Template.explanation.events({
    'click #motd' : function get_motd ()
    {
      $.newt_ajax(
        {
          type: "GET",
          url: "/file/carver/etc/motd?view=read",
          success: function(res){
              // res contains raw file data
              //$('#file').html(res);
              console.log(res);
              Session.set('nersc_response', res);
          },
        });
    }
  });

  Template.explanation.nersc_response = function(){
    return Session.get('nersc_response');
  };

 Template.explanation.mongo_vars = function(){
    step.find({energy: {$exists: true}}).forEach(
        function(myEnergy) {
          console.log(myEnergy.energy);
        }
      );
    var energy = step.find({energy: {$exists: true}}).fetch();
    console.log(energy);
    return energy;
  };

  Template.explanation.events({
    'click #motd' : get_motd
  });
/*
  Template.plot.events = function() {
    

        s = "" +"\n"
                +"\n"
                +"\n  2  1  0  0000  0  0  0  0  0999 V2000"
                +"\n    2.4000   -0.4625    0.0000 C   0  0  0  0  0  0  0  0  0  0  0"
                +"\n    3.7167   -0.4667    0.0000 C   0  0  0  0  0  0  0  0  0  0  0"
                +"\n  2  1  2  0"
                +"\nM  END"
                +"\n"
                +"\n$$$$\n";


tag = $("#plot").jsmol({
    width: 950, 
    height: 950, 
    mol: s,
    color: "white",
    bondWidth: 10,
    zoomScaling: 1.5,
    pinchScaling: 2.0,
    multipleBondSpacing: 2,
    spinRateX: 0.2,
    spinRateY: 0.5,
    spinFPS: 20,
    spin:true});
    // var tag = $("#plot");

    // s = "" +"\n"
    //             +"\n"
    //             +"\n  2  1  0  0000  0  0  0  0  0999 V2000"
    //             +"\n    2.4000   -0.4625    0.0000 C   0  0  0  0  0  0  0  0  0  0  0"
    //             +"\n    3.7167   -0.4667    0.0000 C   0  0  0  0  0  0  0  0  0  0  0"
    //             +"\n  2  1  2  0"
    //             +"\nM  END"
    //             +"\n"
    //             +"\n$$$$\n";

    // tag.jsmol({
    //   width: 500, 
    //   height: 500, 
    //   mol: s,
    //   color: "white",
    //   bondWidth: 10,
    //   zoomScaling: 1.5,
    //   pinchScaling: 2.0,
    //   multipleBondSpacing: 2,
    //   spinRateX: 0.2,
    //   spinRateY: 0.5,
    //   spinFPS: 20,
    //   spin:true
    // });
    // console.log(tag.toString());
    // return tag;
   };
*/
  Template.hero.events({
    'click #tryit' : function jqput(){
       var filename = new Date().getTime();
       console.log(filename);
       $.newt_ajax({type: "PUT",
           url: "/file/hopper/" + NERSC_PATH + "/py/triggers/" + filename,
           data: filename,
           success: function(res){
               // res contains raw file data
              console.log("NEWT File transfer success: " + res.location);
              Session.set('nersc_response', res.status  + " " + res.location);               
           },
       });
  }

    /*
    {
      //var EXEC_COMMAND = PYTHON_PATH + " " + NERSC_PATH + SCRIPT_NAME + " " + NERSC_PATH + OUTPUT_FILE;
      var EXEC_COMMAND =  NERSC_PATH + "/py/exec.sh";
      //var EXEC_COMMAND = "/bin/date";

      $.newt_ajax({type: "POST",
          url: "/command/carver",
          data: {"executable": EXEC_COMMAND},    
          success: function(res){
              // res is { output: "output string", error: "error string", }
              console.log(res.output, res.error);
              Session.set('nersc_response', res.output, res.error);
     
          },
      });
    }
    */
  });

}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
